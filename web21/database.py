# coding=utf-8
import sqlite3
import random
import time
import json
"""
下面是 python 操作 sqlite 数据库的范例代码
注意，代码上课会讲，你不用看懂，也不用运行
"""


class Database():
    def __repr__(self):
        name = self.__class__.__name__
        properties = ('{}=({})'.format(k, v) for k, v in self.__dict__.items())
        s = '\n<{} \n  {}>'.format(name, '\n  '.join(properties))
        return s

    def opendb(self, name ='new.sqlite'):
        db_path = name
        connection = sqlite3.connect(db_path)
        return connection

    def closedb(self, conn):
        return conn.close()

    def comment(self, conn):
        return conn.commit()

    def create(self, conn):
        sql_create = '''
        CREATE TABLE `art` (
            `id`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            `title`    TEXT,
            `summary`    TEXT NOT NULL,
            `img`    TEXT,
            `subtitle`    TEXT,
            `article`    TEXT,
            `tags`    TEXT
        )
        '''
        # 用 execute 执行一条 sql 语句
        print('创建成功')
        return conn.execute(sql_create)

    def insert(conn, title, summary, img, subtitle, article, tags):
        sql_insert = '''
        INSERT INTO
            `art`(`title`,`summary`,`img`,`subtitle`,`article`,`tags`)
        VALUES
            (?, ?, ?, ?, ?, ?);
        '''
        # 参数拼接要用 ?，execute 中的参数传递必须是一个 tuple 类型
        conn.execute(sql_insert, (title, summary, img, subtitle, article, tags))
        return conn.commit()

    def delete(self, conn, id):
        sql_delete = '''
        DELETE FROM
            art
        WHERE
            id=?
        '''
        return conn.execute(sql_delete, (id,))

    def update(conn, id, title, summary, img, subtitle, article, tags):
        """
        UPDATE
            `art`
        SET
            `email`='gua', `username`='瓜'
        WHERE
            `id`=6
        """
        sql_update = '''
        UPDATE
            `art`
        SET
            `title`=?, `summary`=?, `img`=?, `subtitle`=?, `article`=?, `tags`=?
        WHERE
            `id`=?
        '''
        return conn.execute(sql_update, (title, summary, img, subtitle, article, tags, id))

    def select_title(self,conn):
        cursor = conn.cursor()
        sql = '''
        SELECT
            id, title
        FROM
            art
        '''
        m = cursor.execute(sql)
        n =m.fetchall()
        cursor.close()
        return n

    def select_All(self,conn):
        cursor = conn.cursor()
        sql = '''
        SELECT
            *
        FROM
            art
        '''
        m = cursor.execute(sql)
        n =m.fetchall()
        cursor.close()
        return n

    def select_id(self,conn,id):
        cursor = conn.cursor()
        sql= '''
        SELECT
            *
        FROM
            art
        WHERE
            `id`=?
        '''
        m = cursor.execute(sql, (id,))
        n = m.fetchone()
        return n

    def select_range(self,conn,begin,end):
        cursor = conn.cursor()
        sql= '''
        SELECT
            *
        FROM
            art
        WHERE
            `id`>= ? AND `id`<=?
        '''
        m = cursor.execute(sql, (begin,end))
        n =m.fetchall()
        cursor.close()
        return n


def select(conn):
    sql = '''
    SELECT
        id, user_id
    FROM
        session
    '''
    cursor = conn.execute(sql)
    for row in cursor:
        print(row)


def select_bad(conn):
    username = "gua"
    password = "'' OR '1'='1'; DROP TABLE users"
    sql = '''
    SELECT 
      *
    FROM
      users
    WHERE 
      username={} AND password = {};
    '''.format(username, password)

    # SELECT * FROM users WHERE username = '' OR '1'='1';

    cursor = conn.execute(sql)
    for row in cursor:
        print(row)







def main():
    # 指定数据库名字并打开
    a = Database()
    connection = a.opendb()
    print("打开了数据库")
    # list1 = a.select_id(connection,1)
    list1 = a.select_id(connection,30)
    m = json.loads(list1[4])




    print (m)



    # 打开数据库后 就可以用 create 函数创建表
    # a.create(connection)

    # 然后可以用 insert 函数插入数据
    # session_id = ''.join(random.sample('dfjalsdfhqweonbckxv9879132ifnhdkv49y3',8))
    # creat_time = time.time()
    # print('=========time{}==========='.format(type(session_id)))
    # a.insert(connection, session_id, 32423432, creat_time)

    # 可以用 delete 函数删除数据
    # delete(connection, 1)

    # 可以用 update 函数更新数据
    # update(connection, 1, 'gua@cocode.cc')

    # select 函数查询数据
    # select(connection)
    # 必须用 commit 函数提交你的修改
    # 否则你的修改不会被写入数据库
    connection.commit()
    # 用完数据库要关闭
    connection.close()


if __name__ == '__main__':
    main()
