from flask import (
    render_template,
    request,
    redirect,
    session,
    url_for,
    Blueprint,
    make_response,
    send_from_directory,
    abort,
)
from werkzeug.utils import secure_filename
import os
import uuid
import random
import json
import  time

from models.article import Article
from routes import current_user
from utils import log
import operator


main = Blueprint('index', __name__)


"""
用户在这里可以
    访问首页
    注册
    登录

用户登录后, 会写入 session, 并且定向到 /profile
"""


@main.route("/")
def index():

    list11 = []
    m = Article().all()
    m = m[5:15]
    for x in m:
        list11.append(x)
    return render_template('index_list.html',articles = list11, Best=list11 )

# @main.route("/")
# def index():
#
#     list11 = []
#     m = Article().all()
#     # m = m[:-5]
#     for x in m:
#         list11.append(x)
#     return render_template('index.html',articles = list11, Best=list11 )



@main.route("/about")
def about():
    # return render_template("index.html")

    list11 = []
    m = Article().all()
    # m = m[:-8]
    for x in m:
        list11.append(x)
    return render_template('about.html' )


# mm = {
#     'title': '432432',
#     'summary': '23132',
#     'img': '2313',
#     'subtitle': '231',
#     'article': '2132',
#     'tags': '132',
#     'srcid': '13',
# }




#
# @main.route("/login_page")
# def needlogin():
#     return render_template('login_view.html')
#
#
# @main.route("/register", methods=['POST'])
# def register():
#     form = request.form
#     # 用类函数来判断
#     u = User.register(form)
#     return redirect(url_for('.index'))

#
# @main.route("/login", methods=['POST'])
# def login():
#     form = request.form
#     u = User.validate_login(form)
#     print('login u', u)
#     if u is None:
#         # 转到 topic.index 页面
#         return redirect(url_for('topic.index'))
#     else:
#         # session 中写入 user_id
#         session['user_id'] = u.id
#         print('login', session)
#         # 设置 cookie 有效期为 永久
#         session.permanent = True
#         return redirect(url_for('topic.index'))
#
# @main.route("/signout")
# def signout():
#     u = User.find_by(username='来访者')
#     session['user_id'] = u.id
#     return redirect('')
#
#
# @main.route('/profile')
# def profile():
#     u = current_user()
#     if u is None:
#         return redirect(url_for('.index'))
#     else:
#         return render_template('profile.html', user=u)

#
# def valid_suffix(suffix):
#     valid_type = ['jpg', 'png', 'jpeg']
#     return suffix in valid_type
#
#
# @main.route('/image/add', methods=["POST"])
# def add_img():
#     u = current_user()
#
#     # file 是一个上传的文件对象
#     file = request.files['avatar']
#     suffix = file.filename.split('.')[-1]
#     if valid_suffix(suffix):
#         # 上传的文件一定要用 secure_filename 函数过滤一下名字
#         # ../../../../../../../root/.ssh/authorized_keys
#         # filename = secure_filename(file.filename)
#         # 2017/6/14/19/56/yiasduifhy289389f.png
#         # import time
#         # filename = str(time.time()) + filename
#         filename = '{}.{}'.format(str(uuid.uuid4()), suffix)
#         file.save(os.path.join('user_image', filename))
#         User.update(u.id, dict(
#             user_image='/uploads/' + filename
#         ))
#
#     return redirect(url_for(".profile"))
#
#
# # send_from_directory
# # nginx 静态文件
# @main.route("/uploads/<filename>")
# def uploads(filename):
#     return send_from_directory('user_image', filename)
#
#
# @main.route("/rolldice",methods=["POST"])
# def rolldice():
#     m = random.randrange(0,21)
#     return render_template("game/TheDice.html",dicepoint = m)
#
#
# @main.route("/TheDice")
# def TheDice():
#     m = random.randrange(0,21)
#     return render_template("game/TheDice.html")
#
# @main.route("/setting")
# def setting():
#     u = current_user()
#     return render_template("setting.html",user=u)

@main.route('/error')
def error():
    code = int(request.args.get('code'))
    abort(code)



