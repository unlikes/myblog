from flask import (
    render_template,
    request,
    redirect,
    url_for,
    Blueprint,
)

import time
import json
from utils import log
from routes import *
import pickle

from jieba import analyse
import jieba

from models.article import Article

main = Blueprint('article', __name__)


# @main.route("/")
# def index():
#     return render_template("blog/editor.html")


@main.route("/new")
def test1():
    return render_template("blog/editor_markdown.html")


@main.route("/add",methods=["POST"])
def add():
    form =  request.get_json()
    log('==============',form)
    Article().new(form)
    return redirect(url_for('.index'))


@main.route("/add/markdown",methods=["POST"])
def add_markdown():
    form = request.form
    log('==============', form)
    Article().new(form)
    return redirect(url_for('index.index'))


@main.route("/detail/<string:id>")
def detail(id):
    m = Article().find(id)
    log(m)
    return render_template("blog/detail.html",article = m)


@main.route("/del/<string:id>")
def delete(id):
    id = int(id)
    Article().delete(id)
    return redirect(url_for('.index'))


