from flask import (
    render_template,
    request,
    redirect,
    session,
    url_for,
    Blueprint,
    make_response,
    send_from_directory,
    abort,
)
from werkzeug.utils import secure_filename
import os
import uuid
import random
import json
import  time
import jieba

from models.gears import Gears
from routes import current_user
from utils import log
import operator


main = Blueprint('gears', __name__)


"""
用户在这里可以
    访问首页
    注册
    登录

用户登录后, 会写入 session, 并且定向到 /profile
"""


@main.route("/")
def index():


    return render_template('gears/index.html')


@main.route("/search",methods=["GET"])
def search():
    form =  request.args.get('searchtext', '')
    words = form.split(" ")
    gear= Gears.find_words_in_article("gear_name",words)
    return render_template('gears/detail.html',gears = gear )




@main.route("/final")
def detail():
    list11 = []
    m = Gears().find_words_in_article("gear_name",["妈妈"])
    for x in m:
        list11.append(x)
    print(m)
    list11=m[:10]
    return render_template('gears/detail.html',gears = list11 )


@main.route("detail/<string:name>")
def getclass(name):
    name =  name.split("_")
    lis=[]
    print("================")
    for x in name:
        m = Gears.find_words_in_article("gear_class",[x,])
        lis.extend(m)
    return render_template('gears/detail.html',gears = lis )

