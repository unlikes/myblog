import os.path
import time
import json
import sys
import os

def log(*args, **kwargs):
    # time.time() 返回 unix time
    # 如何把 unix time 转换为普通人类可以看懂的格式呢？
    format = '%H:%M:%S'
    value = time.localtime(int(time.time()))
    dt = time.strftime(format, value)
    with open('gua.log.txt', 'a', encoding='utf-8') as f:
        print(dt, *args, **kwargs)
        print(dt, *args, file=f, **kwargs)


def get_files(pathname):
    # 需要在前面定义这些 类似格式为 "data_md/"
    path = os.path.dirname(pathname)
    filelist = []
    for dirpath,dirnames,filenames in os.walk(path):
        for file in filenames:
                m = dirpath+ '/'+file
                n = m.replace('\\',"/")
                # fullpath=os.path.join(dirpath,file)
                # print(n)
                filelist.append(n)
    return filelist



def load(path):
    with open(path, 'r', encoding="utf-8") as f:
        s = f.read()
        # log('load', s)
    return s


def get_file_tags(filepath):
    #截取最前面的和最后面的, 留下中间的目录名作为tags
    fileraw = filepath.split('/')
    tags = fileraw[1:-1]
    # print(tags)
    return tags

# m = get_files("data_md/")
# for x in m:
#     # get_file_tags(x)
#     article = load(x)
#     tags = get_file_tags(x)


# get_files("data_md/")

# from models.article import Article
# print(Article().find_words_in_article("article",["妈妈"]))